from django.shortcuts import render
from lab_1.views import mhs_name, birth_date

#TODO Implement
#Create a content paragraph for your landing page:
landing_page_content = 'Halo, nama saya Rachmat Ridwan. Saat ini saya sedang menempuh pendidikan di Universitas Indonesia.'

def index(request):
    response = {'name': mhs_name, 'content': landing_page_content}
    return render(request, 'lab_2/index_lab2.html', response)
