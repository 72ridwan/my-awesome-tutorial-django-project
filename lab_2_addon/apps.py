from django.apps import AppConfig


class Lab2AddonConfig(AppConfig):
    name = 'lab2_addon'
