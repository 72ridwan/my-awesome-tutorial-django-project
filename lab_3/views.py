from django.shortcuts import render, redirect
from .models import Diary
from datetime import datetime
import pytz
import json
# Create your views here.
diary_dict = {}
anotherDict = {'errorDateBox':'', 'errorActivity':'', 'getWasFromPost': False}

def index(request):
    if (anotherDict['getWasFromPost']):
         anotherDict['getWasFromPost'] = False
    else:
         anotherDict['errorDateBox'] = ''
         anotherDict['errorActivity'] = ''
    diary_dict = Diary.objects.all().values()
    return render(request, 'to_do_list.html', {'diary_dict' : convert_queryset_into_json(diary_dict),
        'errorDateBox': anotherDict['errorDateBox'],
        'errorActivity': anotherDict['errorActivity']})

def add_activity(request):
    if request.method == 'POST':

        if (len(request.POST['activity']) == 0):
            anotherDict['getWasFromPost'] = True
            anotherDict['errorActivity'] = 'Format kegiatan tidak boleh kosong'

        elif (len(request.POST['activity']) > 60):
            anotherDict['getWasFromPost'] = True
            anotherDict['errorActivity'] = 'Format kegiatan tidak boleh melebihi 60 karakter'

        try:
            date = datetime.strptime(request.POST['date'],'%Y-%m-%dT%H:%M')

            if not anotherDict['getWasFromPost']:
                anotherDict['errorDateBox'] = ''
                Diary.objects.create(date=date.replace(tzinfo=pytz.UTC),activity=request.POST['activity'])

        except ValueError:
            anotherDict['getWasFromPost'] = True
            anotherDict['errorDateBox'] = 'Format tanggal tidak tepat'

        return redirect('/lab-3/')

def convert_queryset_into_json(queryset):
    ret_val = []
    for data in queryset:
        ret_val.append(data)
    return ret_val

