$(document).ready(function(){
	$('.my-select').select2();
	
	// Inisialisasi daftar tema
	
	tema = [
    {"id":0,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":3,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
	];

	// Inisialisasi tema ke localStorage, jika belum ada.
	
	if (localStorage.getItem("tema") === null) {
		localStorage.setItem("tema", JSON.stringify(tema));
	}
	
	var temaDipilih = {"Indigo":{"bcgColor":"#3F51B5","fontColor":"#FAFAFA"}};
	
	// Inisialisasi temaDipilih ke localStorage, jika belum ada.
	
	if (localStorage.getItem("temaDipilih") === null) {
		localStorage.setItem("temaDipilih", JSON.stringify(temaDipilih));
  }
	
	//  Muat (load) tema dari local storage, set ke kelas selektor my-select
	
	$('.my-select').select2({data: JSON.parse(localStorage.getItem("tema"))});
	
	//  Menyetel warna background dan font awal-awal dengan apa yang ada di

	
	var retrievedSelected = temaDipilih;
    var bgrColor;
    var fontColor;
	for (k in retrievedSelected) {
        fontColor=retrievedSelected[k].fontColor;
        bgrColor=retrievedSelected[k].bcgColor;
	}
	
	// Set warnanya dengan nilai bawaan dari localStorage saat pertama kali dijalankan
	
	$("body").css({"background-color": bgrColor});
	$("footer").css({"color":fontColor});

	$('.apply-button').on('click', function(){
		var nilaiDipilih = $('.my-select').val();
		var j;
		var temaDipilih = {};
		
		// Cari objek tersebut di dalam kamus JSON tema
		
		for(j in tema){
			if(j==nilaiDipilih){
				var bcgColor = tema[j].bcgColor;
				var fontColor = tema[j].fontColor;
				var text = tema[j].text;
				$("body").css({"background-color": bcgColor});
				  $("footer").css({"color":fontColor});
				temaDipilih[text] = {"bcgColor":bcgColor,"fontColor":fontColor};
				localStorage.setItem('temaDipilih', JSON.stringify(temaDipilih));
			}
		}
	});

	
	var i = 0;
	angleDownSrc = "https://maxcdn.icons8.com/windows10/PNG/16/Arrows/angle_down-16.png";
	angleUpSrc   = "https://maxcdn.icons8.com/windows10/PNG/16/Arrows/angle_up-16.png";
	heightChatBody = parseInt($(".chat-body").css( "height" ));
	widthChatBody  = parseInt($(".chat-body").css( "width" ));
	msgTextClass = "msg-insert";
	
	$("#textinpchat").on("keypress", function(e){		
		if (e.which == 13) {
			var valtext = $("#textinpchat").val().trim();
			if (valtext.length > 0) {
				$(".msg-insert").last().after('<div class="' + msgTextClass + '">' + valtext + '</div>');
			}
			// Mereset textarea
			$("textarea").val("");
			
			// Menghentikan adanya newline setelah fungsi dijalankan
			if(event.preventDefault)
				event.preventDefault();
		}
	}
	
	// Fungsi menutup dan membuka kotak chat beserta animasinya
	)
	var variation = 0;
	$("#btn-expand").click(function() {
		if ($("#btn-expand").attr("src") == angleDownSrc) {
			$("#btn-expand").attr("src", angleUpSrc);
			$(".chat-body").animate({height: '0', margin:'0px 0px 0px 0px'},
				{specialEasing: {margin: "easeInOutExpo", height: "easeInOutExpo"}
				});
			$(".chat-text").animate({height: '0'});
			$(".chat-box").animate({width:'130'},
				{specialEasing: {width: "easeInOutExpo"}}
				);
		} else {
			$("#btn-expand").attr("src", angleDownSrc);
			$(".chat-body").animate({height: ""+heightChatBody, margin:'0px 0px 45px px'},
				{duration:300, specialEasing: {margin: "easeOutBack", height: "easeOutBack"}}
				);
			$(".chat-text").animate({height: '45px'},
				{duration:300, specialEasing: {height: "easeOutBack"}}
				);
			$(".chat-box").animate({width:''+widthChatBody},
				{duration:300, specialEasing: {width: "easeOutBack"}}
				);
		}
	}
	)

	});
	
	// KALKULATOR
	
	var print = document.getElementById('print');
	var erase = false;
	
	// Cek apakah x merupakan operasi matematika atau bukan
	
	function evalOper(x) {
		return (x == '+' || x == '*' || x == '/' || x == '-');
	}
	
	// Evaluasi nilai print terlebih dahulu
	
	function trig() {
		go('eval');
		  if (print.value.len == 0) {
			  print.value = 0;
		  }
	}

	function go(x) {
	  if (x === 'ac') {
		  print.value = "";
	  } else if (x === 'eval') {
		  erase = true;
		  len = print.value.length;
		  
		  // Jika masukannya hanya berupa operasi matematika
		  
		  if (len >= 3 && evalOper(print.value.charAt(len-2))) {
			  print.value = print.value.slice(0, len-3);
		  }
		  
		  if (print.value.length == 0) {
			  return;
		  }
		  
		  print.value = "" + Math.round(evil(print.value) * 10000) / 10000;
		  
		  // Ganti tanda negatif tak berspasi dengan tanda negatif berspasi
		  
		  if (print.value.charAt(0) == '-') {
			print.value = ' - ' + print.value.slice(1, print.value.length);
		  }
	  } else if (x === 'sin'){
		  trig();
		  print.value = Math.sin(print.value*Math.PI/180).toPrecision(3);
		  erase = true;
	   } else if (x === 'tan'){
		  trig();
		  print.value = Math.tan(print.value*Math.PI/180).toPrecision(3);
		  erase = true;
	   } else if (x === 'log') {
		   trig();
		   print.value = (Math.log(print.value)/Math.log(10)).toPrecision(3);
		   erase = true;
	  } else {
			if (x == '.') {
				erase = false;
				
				// Cek apakah salah satu term sudah memiliki koma
				
				useComma = false;
				for (i = print.value.length - 1; i >=0; i--) {
					ch = print.value.charAt(i);
					if (evalOper(ch)) {break;}
					if (ch == '.') {useComma = true; break;}
					}
				if(useComma) {return;}
				
			// JIka masukan tombol kalkulator berupa angka
			
			} else if (x.length != 3){
				if (erase) {erase = false; print.value = "";}
			} else{
				erase = false;
				len = print.value.length;
				
				//  Cegat user dari menambahkan operasi '*' atau '/' yang invalid
				//  seperti saat di awal kalkulator
				
				if ((len == 0 || print.value == " - " || print.value == " + ")
					&& (x.charAt(1) == '*' || x.charAt(1) == '/')) {
					return;
				}
				
				//  Kalau term terakhir di kalkulator adalah operasi matematika,
				//  ganti term itu dengan operasi yang baru
				
				if (evalOper(print.value.charAt(len-2))) {
					print.value = print.value.slice(0, len-3) + x;
					return;
				}
			}
		print.value += x;
	  }
	};

	function evil(fn) {
	  return new Function('return ' + fn)();
	}
