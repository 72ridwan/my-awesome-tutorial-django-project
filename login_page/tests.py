from django.test import TestCase
from django.test import Client
from django.urls import resolve, reverse
from .views import index
from .custom_auth import *
from .csui_helper import *
from .views import index

import requests
import os
import environ

root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')

class LoginPageTestCase(TestCase):
    def test_login_page_exists(self):
        # Not logged in
        client = Client()
        response = client.get('/login/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('login_page/login.html')
        
        # Logged in
        client.post('/login/custom_auth/login/', {'username': env('SSO_USERNAME'), 'password': env('SSO_PASSWORD')})
        response = client.get('/login/')
        self.assertEqual(response.status_code, 302)
    
    def test_login_page_use_login_index_function(self):
        found = resolve('/login/')
        self.assertEqual(found.func, index)
    
    def test_login(self):
        client = Client()
        
        # Fails to log in
        response = client.post('/login/custom_auth/login/', {'username': 'Lorem', 'password': 'ipsum'}, follow=True)
        self.assertIn("Username atau password salah.", response.content.decode('utf8'))
        
        # Succeed to log in
        response = client.post('/login/custom_auth/login/', {'username': env('SSO_USERNAME'), 'password': env('SSO_PASSWORD')})
        self.assertEqual(response.status_code, 302)
    
    def test_login_middleware_redirect_for_previously_unauthorized_access_to_page(self):
        client = Client()
        
        # Test accessing lab 1
        response = client.get(reverse('lab-1:index'))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, reverse('login:index'))
        # Test whether there exists link to redirect to if user suceeds to log in.
        self.assertIn('meta_untuk_login_very_unique_app', client.session.keys())
        
        # Log in
        response = client.post('/login/custom_auth/login/', {'username': env('SSO_USERNAME'), 'password': env('SSO_PASSWORD')})
        
        # Redirect first to log in page
        self.assertEqual(response.url, reverse('login:index'))
        self.assertEqual(response.status_code, 302)
        
        # Then redirect it to the lab 1
        response = client.get(reverse('login:index'))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, reverse('lab-1:index'))
        
    def test_login_logout(self):
        client = Client()
        # Log in
        client.post('/login/custom_auth/login/', {'username': env('SSO_USERNAME'), 'password': env('SSO_PASSWORD')})
        
        # Log out
        response = client.get('/login/custom_auth/logout/')
        self.assertEqual(response.status_code, 302)