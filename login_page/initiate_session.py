import requests
import os
import environ

root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')

def initiate_session(client):
    response = client.get('/login/')
    session = client.session
    session['user_login'] = 'test baru'
    session.save()

def initiate_session_auth(client):
    response = client.post('/login/custom_auth/login/', {'username': env('SSO_USERNAME'), 'password': env('SSO_PASSWORD')})