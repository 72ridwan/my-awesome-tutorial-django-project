from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
response = {}

# Yang dilakukan fungsi ini: memberi flag "masuk index" pada console
# Karena pada pratikum.py sudah ditambahkan middleware
# 'django.contrib.sessions.middleware.SessionMiddleware', request akan
# punya attribute session. Di attribute session ini terdapat keys-keys
# dari sebuah session. Bila user pernah login sebelumnya, request.session.keys()
# akan memiliki keys "user_login" dan akan di-redirect ke halaman profile.
# Bila user sudah login, arahkan ke url lab-4.
# Bila belum, render halaman login.

def index(request):
    if 'user_login' in request.session:
        return HttpResponseRedirect(request.session.get('meta_untuk_login_very_unique_app', '/lab-4/'))
    else:
        print("META IN INDEX", request.META.get('HTTP_REFERER'))
        html = 'login_page/login.html'
        return render(request, html, response)