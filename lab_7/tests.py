"""from django.test import TestCase
from django.test import Client
from django.urls import resolve
import sys
from .views import index, friend_list, friend_detail
from .api_csui_helper.csui_helper_lab7 import CSUIHelperLab7
import json
from .models import Friend
from collections import namedtuple
from login_page.initiate_session import initiate_session
def initiate_friend(client):
    page = 2
    response = client.post('/lab-7/change-page/', {'page':page})
    listOfMhs = json.loads(json.loads(response.content.decode('utf-8'))['listOfMhs'])
    friend = listOfMhs[0]
    return friend

class Lab7Test(TestCase):

    def test_model_can_create_from_addfriend(self):
        self.client = Client()
        initiate_session(self.client)
        friend = initiate_friend(self.client)
        response = self.client.post('/lab-7/add-friend/', {'name':friend['nama'], 'npm':friend['npm']})
        self.assertEqual(Friend.objects.all().count(), 1)
        responseFriendList = self.client.get('/lab-7/get-friend-list/')
        listOfMhs = json.loads(responseFriendList.content.decode('utf-8'))
        print("LIST:", listOfMhs)
        self.assertEqual(listOfMhs['friend_list'][0]['friend_name'].lower(), friend['nama'].lower())

    def test_lab_7_friend_detail_is_exist(self):
        initiate_session(self.client)
        friend = initiate_friend(self.client)
        Friend.objects.create(friend_name=friend['nama'], npm=friend['npm'])
        friend_obj = Friend.objects.all()[0]
        response = self.client.get('/lab-7/get-friend-detail/' + '?id=' + str(friend_obj.id))
        self.assertEqual(response.status_code, 200)

    def setUp(self):
        initiate_session(self.client)
        
    def test_lab_7_loading_fails_with_wrong_credential(self):
        wrong_name = "lorem ipsum"
        wrong_password = "dolor sit amet"
        try:
            csui_helper = CSUIHelperLab7(wrong_name, wrong_password)
        except Exception as e:
            self.assertIn("username atau password sso salah", str(e))
            self.assertIn(wrong_name, str(e))
        
    def test_lab_7_url_is_exist(self):
        response = self.client.get('/lab-7/')
        self.assertEqual(response.status_code, 200)

    def test_lab_7_using_index_func(self):
        found = resolve('/lab-7/')
        self.assertEqual(found.func, index)
    
    def test_lab_7_friend_list_is_exist(self):
        response = self.client.get('/lab-7/friend-list/')
        self.assertEqual(response.status_code, 200)
    
    def test_lab_7_friend_list_using_friend_list_func(self):
        found = resolve('/lab-7/friend-list/')
        self.assertEqual(found.func, friend_list)
    
    def test_lab_7_can_delete_object(self):
        Friend.objects.create(friend_name='Lorem ipsum', npm='1111111111')
        friend_obj = Friend.objects.all()[0]
        response = self.client.post('/lab-7/delete-friend/', {'id':friend_obj.id})
        self.assertEqual(Friend.objects.all().count(), 0)
    
    def test_lab_7_can_validate_existing_npm(self):
        Friend.objects.create(friend_name='Lorem ipsum', npm='1111111111')
        friend_obj = Friend.objects.all()[0]
        response = self.client.post('/lab-7/validate-npm/', {'npm':friend_obj.npm})
        respDict = json.loads(response.content.decode('utf-8'))
        self.assertTrue(respDict['is_taken'])
    
    def test_lab_7_can_validate_unpresent_npm_in_database(self):
        response = self.client.post('/lab-7/validate-npm/', {'npm':'1001'})
        respDict = json.loads(response.content.decode('utf-8'))
        self.assertFalse(respDict['is_taken'])
        
    def test_lab_7_friend_detail_using_friend_detail_func(self):
        found = resolve('/lab-7/get-friend-detail/')
        self.assertEqual(found.func, friend_detail)
        
    def test_model_can_create_new_friend(self):
        self.client = Client()
        initiate_session(self.client)
        friend = initiate_friend(self.client)
        Friend.objects.create(friend_name=friend['nama'], npm=friend['npm'])
        self.assertEqual(Friend.objects.all().count(), 1)
    
    def test_lab_7_cannot_add_invalid_npm(self):
        response = Client().post('/lab-7/add-friend/', {'name':'Lorem', 'npm':'Ipsum'})
        self.assertEqual(Friend.objects.all().count(), 0)
    
    def test_friend_detail_return_none_for_undefined_id(self):
        response = self.client.get('/lab-7/get-friend-detail/')
        self.assertEqual(response.status_code, 302)
        response_two = self.client.get('/lab-7/get-friend-detail/?id=1')
        self.assertEqual(response_two.status_code, 302)
    
    def test_model_cannot_create_existing_friend_from_addfriend(self):
        self.client = Client()
        initiate_session(self.client)
        friend = initiate_friend(self.client)
        Friend.objects.create(friend_name=friend['nama'], npm=friend['npm'])
        response = self.client.post('/lab-7/add-friend/', {'name':friend['nama'], 'npm':friend['npm']})
        self.assertEqual(Friend.objects.all().count(), 1)"""