from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from login_page.initiate_session import initiate_session

class Lab8Test(TestCase):
    def setUp(self):
        initiate_session(self.client)
        
    def test_lab_8_url_is_exist(self):
        response = self.client.get('/lab-8/')
        self.assertEqual(response.status_code, 200)

    def test_lab8_using_index_func(self):
        found = resolve('/lab-8/')
        self.assertEqual(found.func, index)
