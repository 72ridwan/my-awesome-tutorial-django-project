from django.db import models
from django.utils import timezone

class Todo(models.Model):
    def convertTimeZone():
        return timezone.now() + timezone.timedelta(hours=7)

    title = models.CharField(max_length=27)
    description = models.TextField()
    created_date = models.DateTimeField(default=convertTimeZone)

