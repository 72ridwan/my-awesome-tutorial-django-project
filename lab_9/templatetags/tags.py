from django import template
import locale
register = template.Library()

@register.filter(name='rupiah_format')
def rupiah_format(num):
    return "Rp" + "{:,.2f}".format(int(num))