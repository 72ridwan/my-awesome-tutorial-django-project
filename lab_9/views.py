# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
#catatan: tidak bisa menampilkan messages jika bukan menggunakan method 'render'
from .api_enterkomputer import get_items

response = {}
items = ['drone', 'soundcard', 'processor', 'headset']
credential_cookies = {'username': 'Farid Ganteng', 'password': 'Ridwan Ganteng'}

# Yang dilakukan fungsi ini: memberi flag "masuk index" pada console
# Karena pada pratikum.py sudah ditambahkan middleware
# 'django.contrib.sessions.middleware.SessionMiddleware', request akan
# punya attribute session. Di attribute session ini terdapat keys-keys
# dari sebuah session. Bila user pernah login sebelumnya, request.session.keys()
# akan memiliki keys "user_login" dan akan di-redirect ke halaman profile.
# Bila user sudah login, arahkan ke url yg namespace-nya lab-9:profle.
# Bila belum, render halaman login.

def index(request):
    print ("#==> masuk index")
    response['logged_in'] = True
    if 'user_login' in request.session:
        return HttpResponseRedirect(reverse('lab-9:profile'))
    else:
        response['logged_in'] = False
        html = 'lab_9/session/login.html'
        return render(request, html, response)

# Mengambil apa yang ada di request.session serta mengassignnya
# ke response untuk ditampilkan saat dirender.

def set_data_for_session(res, request):
    response['author'] = request.session['user_login']
    response['access_token'] = request.session['access_token']
    response['kode_identitas'] = request.session['kode_identitas']
    response['role'] = request.session['role']
    item_return = []
    print("TEST LAGI")
    for i in items:
        item_dict = {'lists': get_items(i).json()}
        #print ("#drones = ", get_drones().json(), " - response = ", response['drones'])
        ## handling agar tidak error saat pertama kali login (session kosong)
        item_dict['name'] = i
        item_dict['name_capital'] = i.capitalize()
        if i in request.session.keys():
            item_dict['fav'] = request.session[i]
        # jika tidak ditambahkan else, cache akan tetap menyimpan data
        # sebelumnya yang ada pada response, sehingga data tidak up-to-date
        else:
        # jika tidak ditambahkan else, cache akan tetap menyimpan data
        # sebelumnya yang ada pada response, sehingga data tidak up-to-date
            item_dict['fav'] = {}

        item_return.append(item_dict)
    
    response['item_list'] = item_return

# Render template profile. Bila user sudah pernah login,
# pasti di request.sessionnya akan terdapat key 'user_login'
# karena tombol submit di html login page akan memanggil url
# yang di-hook ke custom_auth terlebih dahulu.

def profile(request):
    print ("#==> profile")
    # print("KUNCI", request.session.keys())
    # Mencegah error jika url profile langsung diakses
    if 'user_login' not in request.session.keys():
        return HttpResponseRedirect(reverse('lab-9:index'))
    set_data_for_session(response, request)

    html = 'lab_9/session/profile.html'
    return render(request, html, response)

# Render halaman profile cookie bila sudah login,
# atau render halaman login jika belum login.
def cookie_login(request):
    print ("#==> masuk login")
    if is_login(request):
        return HttpResponseRedirect(reverse('lab-9:cookie_profile'))
    else:
        html = 'lab_9/cookie/login.html'
        return render(request, html, response)

# Otorisasi login cookie, yakni jika username dan password
# sama dengan yang didefine di my_cookie_auth
# Bila sesuai, set key user_login dan user_password
# ke cookie dari redirect ke lab-9:cookie_login

def cookie_auth_login(request):
    print ("# Auth login")
    if request.method == "POST":
        user_login = request.POST['username']
        user_password = request.POST['password']

        if my_cookie_auth(user_login, user_password):
            print ("#SET cookies")
            res = HttpResponseRedirect(reverse('lab-9:cookie_login'))

            res.set_cookie('user_login', user_login)
            res.set_cookie('user_password', user_password)

            return res
        else:
            msg = "Username atau password salah"
            messages.error(request, msg)
            return HttpResponseRedirect(reverse('lab-9:cookie_login'))
    else:
        return HttpResponseRedirect(reverse('lab-9:cookie_login'))

# Render halaman profile jika user sudah login dengan benar.
# Jika usaha login dilakukan dengan manual, perlu dilakukan
# validasi dengan credential_cookies.

def cookie_profile(request):
    print ("# cookie profile ")
    # method ini untuk mencegah error ketika akses URL secara langsung
    if not is_login(request):
        print ("belum login")
        return HttpResponseRedirect(reverse('lab-9:cookie_login'))
    else:
        print ("cookies => ", request.COOKIES)
        in_uname = request.COOKIES['user_login']
        in_pwd= request.COOKIES['user_password']

        # Jika cookie diset secara manual (usaha hacking), distop dengan cara berikut.
        # Agar bisa masuk kembali, maka hapus secara manual cookies yang sudah diset
        if my_cookie_auth(in_uname, in_pwd):
            html = "lab_9/cookie/profile.html"
            res =  render(request, html, response)
            return res
        else:
            print ("#login dulu")
            msg = "Kamu tidak punya akses :P"
            messages.error(request, msg)
            html = "lab_9/cookie/login.html"
            return render(request, html, response)

# Hapus cookie dari kembalian redirect.

def cookie_clear(request):
    res = HttpResponseRedirect('/lab-9/cookie/login')
    res.delete_cookie('lang')
    res.delete_cookie('user_login')
    res.delete_cookie('user_password')

    msg = "Anda berhasil logout. Cookies direset"
    messages.info(request, msg)
    return res

# Otentifikasi username dan password cookie.
def my_cookie_auth(in_uname, in_pwd):
    my_uname = credential_cookies['username']
    my_pwd = credential_cookies['password']
    return in_uname == my_uname and in_pwd == my_pwd

# Mengecek apakah user_login dan user_password ada di cookies.
# Bila ada, artinya user bisa jadi sudah login, karena penambahan
# dua keys tersebut dilakukan bila user melakukan login di halaman login cookie.
def is_login(request):
    return 'user_login' in request.COOKIES and 'user_password' in request.COOKIES

# Fungsi general untuk menambahkan id ke suatu
# dictionary pada parameter key.
def add_session_item(request, key, id):
    if 'user_login' not in request.session:
        messages.error(request, "Kamu harus login terlebih dahulu.")
        return HttpResponseRedirect(reverse('lab-9:index'))
    print ("#ADD session item")
    ssn_key = request.session.keys()
    if not key in ssn_key:
        request.session[key] = [id]
    else:
        items = request.session[key]
        if id not in items:
            items.append(id)
            request.session[key] = items
    print ("setelah = ", key, request.session[key])
    msg = key.capitalize() + " yang dipilih berhasil ditambahkan dalam favorit."
    messages.success(request, msg)
    return HttpResponseRedirect(reverse('lab-9:profile'))

# Fungsi general untuk menghapus id ke suatu
# dictionary pada parameter key.
def del_session_item(request, key, id):
    if 'user_login' not in request.session:
        messages.error(request, "Kamu harus login terlebih dahulu.")
        return HttpResponseRedirect(reverse('lab-9:index'))
    print ("# DEL session item")
    items = request.session[key]
    print ("before = ", items)
    items.remove(id)
    request.session[key] = items
    print ("after = ", items)

    msg = key.capitalize() + " yang dipilih berhasil dihapus dari favorit."
    messages.error(request, msg)
    return HttpResponseRedirect(reverse('lab-9:profile'))

# Fungsi general untuk mereset data dari suatu key.
def clear_session_item(request, key):
    if 'user_login' not in request.session:
        messages.error(request, "Kamu harus login terlebih dahulu.")
        return HttpResponseRedirect(reverse('lab-9:index'))
    
    if (key in request.session.keys()):
        if (len(request.session[key]) != 0):
            msg = "Daftar " + key + " favorit berhasil dihapus."
        else:
            msg = "Daftar " + key + " favorit kosong."
        del request.session[key]
    else:
        msg = "Daftar " + key + " favorit kosong."
            
    
    messages.error(request, msg)
    return HttpResponseRedirect(reverse('lab-9:index'))