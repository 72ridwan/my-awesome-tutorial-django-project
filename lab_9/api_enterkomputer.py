import requests

ITEMS_API       = 'https://www.enterkomputer.com/api/product/'

def get_items(item_name):
    item = requests.get(ITEMS_API + item_name + '.json')
    return item