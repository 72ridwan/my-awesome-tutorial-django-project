from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.http import HttpRequest
from django.contrib import messages
from importlib import import_module

from .views import index, items
from .views import credential_cookies as crdntl
from .csui_helper import *
from .api_enterkomputer import get_items
from .custom_auth import *
from .templatetags.tags import rupiah_format

import requests
import os
import environ
import json


root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')

class Lab9Tests(TestCase):
    def test_lab_9_url_is_exist(self):
        response = Client().get('/lab-9/')
        self.assertEqual(response.status_code, 200)
    
    def test_lab_9_using_index_func(self):
        found = resolve('/lab-9/')
        self.assertEqual(found.func, index)
        
    def test_lab9_using_right_template(self):
        # Jika belum login
        response = self.client.get('/lab-9/')
        self.assertTemplateUsed(response, 'lab_9/session/login.html')
        # Jika sudah login
        session = self.client.session
        session['user_login'] = 'test baru'
        session.save()
        response = self.client.get('/lab-9/')
        self.assertEqual(response.status_code, 302)
    
    def test_login_cookie_page(self):
        # Login
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")
        response_post = self.client.post(reverse('lab-9:auth_login'), {'username': self.username, 'password': self.password})
        
        # Test jika method yang digunakan pada cookie_auth_login bukan post
        response_post = self.client.get(reverse('lab-9:cookie_auth_login'))
        self.assertEqual(response_post.status_code, 302)
        
        # Test jika halaman profile cookie diakses tanpa login
        response_post = self.client.get(reverse('lab-9:cookie_profile'))
        self.assertEqual(response_post.status_code, 302)
        
        # Test jika username dan password salah
        response_post = self.client.post(reverse('lab-9:cookie_auth_login'), {'username': 'Lorem', 'password': 'ipsum'})
        response_post = self.client.get(reverse('lab-9:cookie_login'))
        html_response = response_post.content.decode('utf8')
        self.assertIn('Username atau password salah',html_response)
        
        # Test login pada halaman cookie dengan data yang valid
        self.client.post(reverse('lab-9:cookie_auth_login'), {'username': crdntl['username'], 'password': crdntl['password']})
        self.client.get(reverse('lab-9:cookie_login'))
        response_post = self.client.get(reverse('lab-9:cookie_profile'))
        self.assertTemplateUsed(response_post, 'lab_9/cookie/profile.html')
        
        # Test jika cookie diset secara manual (usaha hacking)
        response = self.client.get(reverse('lab-9:cookie_profile'))
        response.client.cookies['user_login'] = 'usaha hacking tidak berkah'
        response_post = self.client.get(reverse('lab-9:cookie_profile'))
        self.assertTemplateUsed(response_post, 'lab_9/cookie/login.html')
        
        # Test logout halaman cookie
        response_post = self.client.post(reverse('lab-9:cookie_auth_login'), {'username': crdntl['username'], 'password': crdntl['password']})
        response_post = self.client.get(reverse('lab-9:cookie_clear'))
        self.assertEqual(response_post.status_code, 302)

    def test_profile_logged_in(self):
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")
        response_post = self.client.post(reverse('lab-9:auth_login'), {'username': self.username, 'password': self.password})
        response = self.client.get('/lab-9/profile/')
        html_response = response.content.decode('utf8')
        self.assertIn(self.username,html_response)

    def test_profile_not_login(self):
        response = self.client.get('/lab-9/profile/')
        self.assertEqual(response.status_code, 302)

    def test_add_delete_and_reset_items(self):
        # Log in
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")
        response_post = self.client.post(reverse('lab-9:auth_login'), {'username': self.username, 'password': self.password})
        
        for k in items:
            lst = get_items(k).json()
            
            # Mereset favorit item saat daftar favoritnya tidak ada di session
            response_post = self.client.post(reverse('lab-9:clear_session_item', kwargs={'key':k}))
            response = self.client.get('/lab-9/profile/')
            html_response = response.content.decode('utf8')
            self.assertIn("Daftar " + k + " favorit kosong.",html_response)
            
            # Tambah dua item ke list favorit item
            for i in range(2):
                response_post = self.client.post(reverse('lab-9:add_session_item', kwargs={'id':lst[i]['id'],'key':k}))
            
            # Hapus item pertama dari favorit item
            response_post = self.client.post(reverse('lab-9:del_session_item', kwargs={'id':lst[0]['id'],'key':k}))
            response = self.client.get('/lab-9/profile/')
            html_response = response.content.decode('utf8')
            self.assertIn(k.capitalize() + " yang dipilih berhasil dihapus dari favorit.",html_response)
            
            # Reset daftar favorit item
            response_post = self.client.post(reverse('lab-9:clear_session_item', kwargs={'key':k}))
            response = self.client.get('/lab-9/profile/')
            html_response = response.content.decode('utf8')
            self.assertIn("Daftar " + k + " favorit berhasil dihapus.",html_response)
            
            # Mereset favorit item saat daftar favorit masih terdefinisi tapi kosong
            self.client.post(reverse('lab-9:add_session_item', kwargs={'id':lst[0]['id'],'key':k}))
            self.client.post(reverse('lab-9:del_session_item', kwargs={'id':lst[0]['id'],'key':k}))
            self.client.get('/lab-9/profile/')
            self.client.post(reverse('lab-9:clear_session_item', kwargs={'key':k}))
            response = self.client.get('/lab-9/profile/')
            html_response = response.content.decode('utf8')
            self.assertIn("Daftar " + k + " favorit kosong.",html_response)
    
    def test_unable_to_modify_fav_items_without_login(self):
        # Test tambah item favorit
        response = self.client.post(reverse('lab-9:add_session_item', kwargs={'id':'111','key':'randomkey'}), follow=True)
        html_response = response.content.decode('utf8')
        self.assertIn("Kamu harus login terlebih dahulu.", html_response)
        
        # Test hapus item favorit
        response = self.client.post(reverse('lab-9:del_session_item', kwargs={'id':'111','key':'randomkey'}), follow=True)
        html_response = response.content.decode('utf8')
        self.assertIn("Kamu harus login terlebih dahulu.", html_response)
        
        # Test reset item favorit
        response = self.client.post(reverse('lab-9:clear_session_item', kwargs={'key':'randomkey'}), follow=True)
        html_response = response.content.decode('utf8')
        self.assertIn("Kamu harus login terlebih dahulu.", html_response)
        
    def test_api_enterkomputer(self):
        for k in items:
            response = requests.get('https://www.enterkomputer.com/api/product/' + k + '.json')
            self.assertEqual(response.json(),get_items(k).json())
    
    def test_csui_helper_verify_function(self):
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")
        access_token = get_access_token(self.username,self.password)
        parameters = {"access_token": access_token, "client_id": get_client_id()}
        response = requests.get(API_VERIFY_USER, params=parameters)
        result = verify_user(access_token)
        self.assertEqual(result,response.json())
    
    def test_csui_helper_get_data_user(self):
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")

        access_token = get_access_token(self.username,self.password)
        parameters = {"access_token": access_token, "client_id": get_client_id()}

        npm = json.loads(requests.get(API_VERIFY_USER, params=parameters).content)["identity_number"]
        response = requests.get(API_MAHASISWA+npm, params=parameters)

        result = get_data_user(access_token,npm)
        self.assertEqual(result,response.json())
    
    def test_login_auth(self):
        response_post = self.client.post(reverse('lab-9:auth_login'), {'username': env("SSO_USERNAME"), 'password': env("SSO_PASSWORD")})
        self.assertEqual(response_post.status_code, 302)

    def test_fail_login(self):
        response_post = self.client.post(reverse('lab-9:auth_login'), {'username': 'Lorem', 'password': 'ipsum'})
        response = self.client.get('/lab-9/')
        html_response = response.content.decode('utf8')
        self.assertIn('Username atau password salah.',html_response)

    def test_logout_auth(self):
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")
        response_post = self.client.post(reverse('lab-9:auth_login'), {'username': self.username, 'password': self.password})
        response = self.client.post(reverse('lab-9:auth_logout'))
        response = self.client.get('/lab-9/')
        html_response = response.content.decode('utf8')
        self.assertIn("Anda berhasil keluar. Semua session Anda sudah dihapus.", html_response)
    
    def test_rupiah_format(self):
        self.assertEqual("Rp1,000.00", rupiah_format(1000))
        