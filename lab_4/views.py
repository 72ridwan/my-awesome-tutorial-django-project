from django.shortcuts import render
from lab_2.views import landing_page_content
from django.http import HttpResponseRedirect
from .forms import Message_Form
from .models import Message

# Create your views here.
response = {'author': "Rachmat Ridwan"} #TODO Implement yourname
about_me = ["Saya Rachmat Ridwan, biasa dipanggil Farid",
            "Kini sedang pendidikan di Universitas Indonesia",
            "Beberapa anime membentuk jati diri saya, seperti Nichijou dan SAO",
            "Saya juga amat suka komposisi musik orkestra",
            "Sampai saat ini saya senang mengamati latar belakang musik film",
            "Saya juga senang membuat gambar 3D dan animasi"
            ]

errorNoMessage    = "Cek isi pesan Anda, apakah sudah terisi atau belum."
errorInvalidEmail = "Cek email yang Anda gunakan karena e-mail harus mengandung @."
errorLongName     = "Gunakan nama yang tidak terlalu panjang (panjang nama maksimal 27 karakter)"
errorLongMessage  = "Cek panjang pesan Anda (panjang pesan maksimal 1000 karakter)"
errorOpening      = "Pesan Anda tidak dapat diproses"
errorOpeningTwo   = "Padahal saya penasaran sekali apa isi pesan Anda. Coba:"

errorDict = {'errorForm' : {}, 'getWasFromPost' : False, 'errorOpening':'', 'errorOpeningTwo':''}

def index(request):
    response['content'] = landing_page_content
    html = 'lab_4/lab_4.html'

    if errorDict['getWasFromPost']:
        errorDict['getWasFromPost'] = False
    else:
        errorDict['errorOpening'] = ''
        errorDict['errorOpeningTwo'] = ''
        errorDict['errorForm'] = {}

    response['about_me'] = about_me
    response['message_form'] = Message_Form
    response['errorForm'] = errorDict['errorForm']
    response['errorOpening'] = errorDict['errorOpening']
    response['errorOpeningTwo'] = errorDict['errorOpeningTwo']

    return render(request, html, response)

def message_post(request):
    errorDict['errorForm'] = {}
    form = Message_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        errorDict['getWasFromPost'] = False
        response['name'] = request.POST['name'] if request.POST['name'] != "" else "Anonymous"
        response['email'] = request.POST['email'] if request.POST['email'] != "" else "Anonymous"
        response['message'] = request.POST['message']
        message = Message(name=response['name'], email=response['email'],
                          message=response['message'])
        message.save()
        html ='lab_4/form_result.html'
        return render(request, html, response)
    else:
        errorDict['getWasFromPost'] = True
        if len(request.POST['email']) != 0 and "@" not in request.POST['email']:
            errorDict['errorForm']['invalidEmail'] = errorInvalidEmail
        if len(request.POST['name']) > 27:
            errorDict['errorForm']['longName'] = errorLongName
        if len(request.POST['message']) > 1000:
            errorDict['errorForm']['longMessage'] = errorLongMessage
        if len(request.POST['message']) == 0:
            errorDict['errorForm']['longMessage'] = errorNoMessage
        errorDict['errorOpening'] = errorOpening
        errorDict['errorOpeningTwo'] = errorOpeningTwo
        return HttpResponseRedirect('/lab-4/')

def message_table(request):
    message = Message.objects.all()
    response['message'] = message
    html = 'lab_4/table.html'
    return render(request, html , response)



