from django import forms

class Message_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
        'invalid': 'Isi input dengan email',
    }
    attrs = {
        'class': 'form-control',
    }

    attrsNama = attrs.copy()
    attrsNama.update({'placeholder': 'Cth: Rachmat Ridwan'})
    attrsEmail = attrs.copy()
    attrsEmail.update({'placeholder': 'Cth: contoh@contoh.com'})
    attrsPesan = attrs.copy()
    attrsPesan.update({'placeholder': 'Maks 1000 karakter'})

    name = forms.CharField(label='Nama', required=False, max_length=27, empty_value='Anonymous', widget=forms.TextInput(attrs=attrsNama))
    email = forms.EmailField(required=False, widget=forms.EmailInput(attrs=attrsEmail))
    message = forms.CharField(max_length=1000, widget=forms.Textarea(attrs=attrsPesan), required=True)

