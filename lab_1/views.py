from django.shortcuts import render
from datetime import datetime, date

# Enter your name here
mhs_name = "Rachmat Ridwan"
birth_date = datetime(1998, 10, 30, 0, 00)

curr_year = int(datetime.now().strftime("%Y"))

# Create your views here.
def index(request):
    response = {'author': mhs_name, 'age': calculate_age(birth_date.year)}
    return render(request, 'lab_1/index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
